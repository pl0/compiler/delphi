unit pl0com;

{$IFDEF FPC}
  {$MODE Delphi}
{$ENDIF}

interface

uses
{$IFNDEF FPC}
   System.SysUtils;
{$ELSE}
       SysUtils;
{$ENDIF}

const
  norw = 11; { no. of reserved words }
  txmax = 100; { length of identifier table }
  nmax = 14; { max. no. of digits in numbers }
  al = 10; { length of identifiers }
  amax = 2047; { maximum address }
  levmax = 3; { maximum depth of block nesting }
{        cxmax = 200;} { size of code array }
  stacksize = 500; {size of stack}

type
  symbol = (nul, ident, number, plus, minus, times, slash, oddsym, eql,
    neq, lss, leq, gtr, geq, lparen, rparen, comma, semicolon, period,
    becomes, beginsym, endsym, ifsym, thensym, whilesym, dosym, callsym,
    constsym, varsym, procsym);
  alfa = packed array [1 .. al] of char;
  pl0object = (constant, varible, proc);
  symset = set of symbol;
  fct = (lit, opr, lod, sto, cal, ink, jmp, jpc); { functions }
  terrortxt = packed array[1 .. 100] of String; { text for error codes }

  instruction = packed record
          f: fct; { function code }
          l: 0 .. levmax; { level }
          a: 0 .. amax { displacement address }
          end;

          { lit 0,a  :  load constant a
            opr 0,a  :  execute operation a
            lod l,a  :  load varible l,a
            sto l,a  :  store varible l,a
            cal l,a  :  call procedure a at level l
            ink 0,a  :  inc(k)rement t-register by a
            jmp 0,a  :  jump to a
            jpc 0,a  :  jump conditional to a }
var
  ch: char; { last character read }
  sym: symbol; { last symbol read }
  id: alfa; { last identifier read }
  num: integer; { last number read }
  cc: integer; { character count }
  ll: integer; { line length }
  kk, err: integer;
  cx: integer; { code allocation index }
  line: array [1 .. 81] of char;
  a: alfa;
  {                code: array [0 .. cxmax] of instruction;}
  code: array of instruction; { code array without fixed length }
  word: array [1 .. norw] of alfa;
  wsym: array [1 .. norw] of symbol;
  ssym: array [char] of symbol;
  mnemonic: array [fct] of packed array [1 .. 5] of char;
  stdoutmnemonic: array [fct] of packed array [1 .. 3] of char;
  declbegsys, statbegsys, facbegsys: symset;
  table: array [0 .. txmax] of record name: alfa;
  case kind: pl0object of
          constant:
                  (val: integer);
          varible, proc:
                  (level, adr: integer)
  end;
  errortxt: terrortxt;

procedure gen(x: fct; y, z: integer);
procedure interpret;
function getfct(s: string): fct;

implementation

function getfct(s: string): fct;
begin
  s := LowerCase(s);
  if (s = 'lit') or (s = '0') then
          getfct := fct.lit {opcode 0}
  else if (s = 'opr') or (s = '1') then
          getfct := fct.opr {opcode 1}
  else if (s = 'lod') or (s = '2') then
          getfct := fct.lod {opcode 2}
  else if (s = 'sto') or (s = '3') then
          getfct := fct.sto {opcode 3}
  else if (s = 'cal') or (s = '4') then
          getfct := fct.cal {opcode 4}
  else if (s = 'ink') or (s = '5') then
          getfct := fct.ink {opcode 5}
  else if (s = 'jmp') or (s = '6') then
          getfct := fct.jmp {opcode 6}
  else if (s = 'jpc') or (s = '7') then
          getfct := fct.jpc {opcode 7}
  else
    begin
      ExitCode := 100;
      Writeln(ErrOutput, 'Illegal instruction or opcode.');
      RunError(ExitCode)
    end;
end;

procedure gen(x: fct; y, z: integer);
begin
  SetLength(code, cx+1);
  with code[cx] do
    begin
      f := x;
      l := y;
      a := z
    end;
  cx := cx + 1;
end { gen };

procedure interpret;
var
  p, b, t: integer; { program-, base-, topstack-registers }
  i: instruction; { instruction register }
  s: array [1 .. stacksize] of integer; { datastore }

  function base(l: integer): integer;
  var
    b1: integer;
  begin
    b1 := b; { find base l levels down }
    while l > 0 do
    begin
            b1 := s[b1];
            l := l - 1
    end;
    base := b1
  end { base };

begin
  writeln(ErrOutput,'Start PL/0');
  t := 0;
  b := 1;
  p := 0;
  s[1] := 0;
  s[2] := 0;
  s[3] := 0;
  repeat
    i := code[p];
    p := p + 1;
    with i do
      case f of
        lit:
          begin
            t := t + 1;
            s[t] := a
          end;
        opr:
          case a of { operator }
            0:
              begin { return }
                t := b - 1;
                p := s[t + 3];
                b := s[t + 2];
              end;
            1:
              s[t] := -s[t];
            2:
              begin
                t := t - 1;
                s[t] := s[t] + s[t + 1]
              end;
            3:
              begin
                t := t - 1;
                s[t] := s[t] - s[t + 1]
              end;
            4:
              begin
                t := t - 1;
                s[t] := s[t] * s[t + 1]
              end;
            5:
              begin
                t := t - 1;
                s[t] := s[t] div s[t + 1]
              end;
            6:
              s[t] := ord(odd(s[t]));
            8:
              begin
                t := t - 1;
                s[t] := ord(s[t] = s[t + 1])
              end;
            9:
              begin
                t := t - 1;
                s[t] := ord(s[t] <> s[t + 1])
              end;
            10:
              begin
                t := t - 1;
                s[t] := ord(s[t] < s[t + 1])
              end;
            11:
              begin
                t := t - 1;
                s[t] := ord(s[t] >= s[t + 1])
              end;
            12:
              begin
                t := t - 1;
                s[t] := ord(s[t] > s[t + 1])
              end;
            13:
              begin
                t := t - 1;
                s[t] := ord(s[t] <= s[t + 1])
              end;
          end;
        lod:
          begin
            t := t + 1;
            s[t] := s[base(l) + a]
          end;
        sto:
          begin
            s[base(l) + a] := s[t];
            writeln(ErrOutput,s[t]);
            t := t - 1
          end;
        cal:
          begin { generate new block mark }
            s[t + 1] := base(l);
            s[t + 2] := b;
            s[t + 3] := p;
            b := t + 1;
            p := a
          end;
        ink:
          t := t + a;
        jmp:
          p := a;
        jpc:
          begin
            if s[t] = 0 then
            p := a;
            t := t - 1
          end
      end { with, case }
  until p = 0;
  writeln(ErrOutput,'End PL/0');
end { interpret };

end.
