program pl0;

{$IFDEF FPC}
{$MODE Delphi}
{$ENDIF}

{$APPTYPE CONSOLE}

{$IFNDEF FPC}
{$R *.res}
{$ENDIF}

uses
  {$IFNDEF FPC}
  System.SysUtils,
  {$ELSE}
  SysUtils,
  {$ENDIF }
  pl0com in 'pl0com.pas';

var
  readLine: String;
  charSep: Array [0 .. 0] of Char;
  aFile: Text;
  aFileName: String;
{$IFNDEF FPC}
  codeStr: System.TArray<System.String>;
{$ELSE}
  codeStr: TStringArray;
{$ENDIF}

begin
  cx := 0;
  charSep := ' ';

  try
    aFileName := ParamStr(1);
    Assign(aFile, aFileName);
    Reset(aFile);
    { Readln(aFile, readLine); }
    while not Eof(aFile) do
    begin
      Readln(aFile, readLine);
      codeStr := readLine.Split(charSep);
      gen(getfct(codeStr[0]), StrToInt(codeStr[1]), StrToInt(codeStr[2]));
    end;
    interpret;

  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;

end.
